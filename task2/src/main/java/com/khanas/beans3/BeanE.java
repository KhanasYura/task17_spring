package com.khanas.beans3;

import org.springframework.stereotype.Component;

@Component
public class BeanE {

    private String name = "BeanE";
    private int value = 5;

    public final String getName() {
        return name;
    }

    public final void setName(final String name) {
        this.name = name;
    }

    public final int getValue() {
        return value;
    }

    public final void setValue(final int value) {
        this.value = value;
    }

    @Override
    public final String toString() {
        return "BeanE{"
                + "name='" + name + '\''
                + ", value=" + value + '}';
    }

}