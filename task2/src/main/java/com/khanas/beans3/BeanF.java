package com.khanas.beans3;

import org.springframework.stereotype.Component;

@Component
public class BeanF {

    private String name = "BeanF";
    private int value = 6;

    public final String getName() {
        return name;
    }

    public final void setName(final String name) {
        this.name = name;
    }

    public final int getValue() {
        return value;
    }

    public final void setValue(final int value) {
        this.value = value;
    }

    @Override
    public final String toString() {
        return "BeanF{"
                + "name='" + name + '\''
                + ", value=" + value
                + '}';
    }

}
