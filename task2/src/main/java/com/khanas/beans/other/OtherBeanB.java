package com.khanas.beans.other;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class OtherBeanB {
    @Override
    public final String toString() {
        return "OtherBeanB";
    }
}
