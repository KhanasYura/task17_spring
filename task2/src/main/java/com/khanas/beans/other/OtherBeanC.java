package com.khanas.beans.other;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Qualifier("qualifier")
@Scope("prototype")
public class OtherBeanC {
    @Override
    public final String toString() {
        return "OtherBeanC";
    }
}
