package com.khanas.beans.other;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("singleton")
public class OtherBeanA {
    @Override
    public final String toString() {
        return "OtherBeanA";
    }
}
