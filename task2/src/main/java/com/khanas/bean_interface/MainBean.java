package com.khanas.bean_interface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MainBean {

    @Autowired
    private List<BeanInterface> beans;

    public final void printBeans() {
        for (BeanInterface bean : beans) {
            bean.hi();
        }
    }

    @Override
    public final String toString() {
        return "MainBean{"
                + "bean1=" + beans.get(0).hi()
                + ", bean2=" + beans.get(1).hi()
                + ", bean3=" + beans.get(2).hi() + '}';
    }
}
