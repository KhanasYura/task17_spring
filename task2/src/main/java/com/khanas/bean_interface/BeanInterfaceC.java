package com.khanas.bean_interface;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(3)
public class BeanInterfaceC implements BeanInterface {

    @Override
    public final String hi() {
        return "Hi from BeanInterfaceC";
    }
}