package com.khanas.bean_interface;

import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
@Primary
public class BeanInterfaceA implements BeanInterface {

    @Override
    public final String hi() {
        return "Hi from BeanInterfaceA";
    }
}
