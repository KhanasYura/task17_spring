package com.khanas.bean_interface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class AnotherBean {

    @Autowired
    private BeanInterface beanA;

    @Autowired
    @Qualifier("beanInterfaceB")
    private BeanInterface beanB;

    @Autowired
    @Qualifier("beanInterfaceC")
    private BeanInterface beanC;

    @Override
    public final String toString() {
        return "AnotherBean{"
                + "beanA=" + beanA.hi()
                + ", beanB=" + beanB.hi()
                + ", beanC=" + beanC.hi() + '}';
    }
}
