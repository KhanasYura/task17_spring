package com.khanas.bean_interface;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
public class BeanInterfaceB implements BeanInterface {

    @Override
    public final String hi() {
        return "Hi from BeanInterfaceB";
    }
}