package com.khanas;

import com.khanas.beans.other.OtherBeanA;
import com.khanas.beans.other.OtherBeanB;
import com.khanas.beans.other.OtherBeanC;
import com.khanas.config.Config;
import com.khanas.config.Config2;
import com.khanas.config.Config3;
import com.khanas.config.Config4;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {

    private static Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(Config.class, Config2.class, Config3.class, Config4.class);

        for (String bean : context.getBeanDefinitionNames()) {
            logger.info(context.getBean(bean).toString());
        }

        logger.info("HashCodes");
        logger.info(context.getBean(OtherBeanA.class).hashCode());
        logger.info(context.getBean(OtherBeanA.class).hashCode());
        logger.info(context.getBean(OtherBeanB.class).hashCode());
        logger.info(context.getBean(OtherBeanB.class).hashCode());
        logger.info(context.getBean(OtherBeanC.class).hashCode());
        logger.info(context.getBean(OtherBeanC.class).hashCode());

    }
}
