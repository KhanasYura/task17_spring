package com.khanas.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.khanas.dependency_injection")
@ComponentScan("com.khanas.beans.other")
public class Config3 {
}
