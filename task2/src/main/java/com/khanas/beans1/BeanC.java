package com.khanas.beans1;

import org.springframework.stereotype.Component;

@Component
public class BeanC {

    private String name = "BeanC";
    private int value = 3;

    public final String getName() {
        return name;
    }

    public final void setName(final String name) {
        this.name = name;
    }

    public final int getValue() {
        return value;
    }

    public final void setValue(final int value) {
        this.value = value;
    }

    @Override
    public final String toString() {
        return "BeanC{"
                + "name='" + name + '\''
                + ", value=" + value + '}';
    }

}
