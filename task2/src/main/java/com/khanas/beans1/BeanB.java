package com.khanas.beans1;

import org.springframework.stereotype.Component;

@Component
public class BeanB {

    private String name = "BeanB";
    private int value = 2;

    public final String getName() {
        return name;
    }

    public final void setName(final String name) {
        this.name = name;
    }

    public final int getValue() {
        return value;
    }

    public final void setValue(final int value) {
        this.value = value;
    }

    @Override
    public final String toString() {
        return "BeanB{"
                + "name='" + name + '\''
                + ", value=" + value + '}';
    }

}
