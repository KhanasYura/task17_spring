package com.khanas.dependency_injection;

import com.khanas.beans.other.OtherBeanB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Constructor {

    private OtherBeanB otherBeanB;

    @Autowired
    public Constructor(OtherBeanB otherBeanB){
        this.otherBeanB = otherBeanB;
    }

    @Override
    public String toString() {
        return "Constructor{"
                + "otherBeanB=" + otherBeanB + '}';
    }
}
