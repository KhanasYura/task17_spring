package com.khanas.dependency_injection;

import com.khanas.beans.other.OtherBeanA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Field {

    @Autowired
    private OtherBeanA otherBeanA;

    @Override
    public String toString() {
        return "Field{"
                + "otherBeanA=" + otherBeanA + '}';
    }
}
