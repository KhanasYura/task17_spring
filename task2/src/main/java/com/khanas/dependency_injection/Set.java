package com.khanas.dependency_injection;

import com.khanas.beans.other.OtherBeanC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Set {

    private OtherBeanC bean;

    @Autowired
    @Qualifier("qualifier")
    public void setOtherBeanC(OtherBeanC otherBeanC) {
        this.bean = otherBeanC;
    }

    @Override
    public String toString() {
        return "Set{"
                + "bean=" + bean + '}';
    }
}
