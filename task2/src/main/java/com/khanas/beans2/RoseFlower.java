package com.khanas.beans2;

import org.springframework.stereotype.Component;

@Component
public class RoseFlower {

    private String name = "Rose";
    private int amount = 5;

    @Override
    public final String toString() {
        return "NarcissusFlower{"
                + "name='" + name + '\''
                + ", amount=" + amount + '}';
    }
}
