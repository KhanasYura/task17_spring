package com.khanas.beans2;

import org.springframework.stereotype.Component;

@Component
public class CatAnimal {
    private String name = "Cat";
    private int year = 2010;

    @Override
    public final String toString() {
        return "CatAnimal{"
                + "name='" + name + '\''
                + ", year=" + year + '}';
    }
}
