package com.khanas;

import com.khanas.beans.*;
import com.khanas.configuration.BeanConfigOne;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {

    private static Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(BeanConfigOne.class);

        BeanA beanA = context.getBean(BeanA.class);
        logger.info(beanA.toString());

        BeanB beanB = context.getBean(BeanB.class);
        logger.info(beanB.toString());

        BeanC beanC = context.getBean(BeanC.class);
        logger.info(beanC.toString());

        BeanD beanD = context.getBean(BeanD.class);
        logger.info(beanD.toString());

        BeanE beanE = context.getBean(BeanE.class);
        logger.info(beanE.toString());

        BeanF beanF = context.getBean(BeanF.class);
        logger.info(beanF.toString());

        MyBean bean = context.getBean(MyBean.class);
        logger.info(bean.toString());

        BeanValid beanValid = context.getBean(BeanValid.class);
        logger.info(beanValid.toString());

        context.close();
    }
}
