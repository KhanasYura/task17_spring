package com.khanas.beans;

import com.khanas.validator.BeanValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BeanF implements BeanValidator {

    private Logger logger = LogManager.getLogger();

    private String name;
    private int value;

    public BeanF(final String name, final int value) {
        this.name = name;
        this.value = value;
    }

    public final String getName() {
        return name;
    }

    public final void setName(final String name) {
        this.name = name;
    }

    public final int getValue() {
        return value;
    }

    public final void setValue(final int value) {
        this.value = value;
    }

    @Override
    public final String toString() {
        return "BeanF{"
                + "name='" + name + '\''
                + ", value=" + value
                + '}';
    }

    public final void validate() {
        if (name == null) {
            logger.info("Name in BeanF cannot be null");
        }
        if (value < 0) {
            logger.info("Value in BeanF cannot be negative");
        }
    }
}
