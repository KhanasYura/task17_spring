package com.khanas.beans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;



public class MyBean implements BeanFactoryPostProcessor {

    private Logger logger = LogManager.getLogger();

    @Override
    public final void postProcessBeanFactory(final ConfigurableListableBeanFactory beanFactory) throws BeansException {
        for (String beanName : beanFactory.getBeanDefinitionNames()) {
            if (beanName.equals("beanB")) {
                BeanDefinition beanDefinition = beanFactory.getBeanDefinition(beanName);
                logger.info("old init: " + beanDefinition.getInitMethodName());
                beanDefinition.setInitMethodName("init2");
                logger.info("New init: " + beanDefinition.getInitMethodName());
            }
        }
    }

    @Override
    public final String toString() {
        return "MyBean";
    }
}
