package com.khanas.beans;

import com.khanas.validator.BeanValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class BeanE implements BeanValidator {

    private Logger logger = LogManager.getLogger();

    private String name;
    private int value;

    public BeanE(final String name, final int value) {
        this.name = name;
        this.value = value;
    }

    public final String getName() {
        return name;
    }

    public final void setName(final String name) {
        this.name = name;
    }

    public final int getValue() {
        return value;
    }

    public final void setValue(final int value) {
        this.value = value;
    }

    @PostConstruct
    public final void annoInitMethod() {
        logger.info("inside @PostConstruct-method BeanE");
    }

    @PreDestroy
    public final void annoDestroyMethod() {
        logger.info("inside @PreDestroy-method BeanE");
    }

    @Override
    public final String toString() {
        return "BeanE{"
                + "name='" + name + '\''
                + ", value=" + value + '}';
    }

    public final void validate() {
        if (name == null) {
            logger.info("Name in BeanE cannot be null");
        }
        if (value < 0) {
            logger.info("Value in BeanE cannot be negative");
        }
    }
}