package com.khanas.beans;

import com.khanas.validator.BeanValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BeanD implements BeanValidator {

    private Logger logger = LogManager.getLogger();

    private String name;
    private int value;

    public BeanD(final String name, final int value) {
        this.name = name;
        this.value = value;
    }

    public final void init() {
        logger.info("Init beanD");
    }

    public final void destroy() {
        logger.info("Destroy beanD");
    }

    public final String getName() {
        return name;
    }

    public final void setName(final String name) {
        this.name = name;
    }

    public final int getValue() {
        return value;
    }

    public final void setValue(final int value) {
        this.value = value;
    }

    @Override
    public final String toString() {
        return "BeanD{"
                + "name='" + name + '\''
                + ", value=" + value + '}';
    }

    public final void validate() {
        if (name == null) {
            logger.info("Name in BeanD cannot be null");
        }
        if (value < 0) {
            logger.info("Value in BeanD cannot be negative");
        }
    }
}
