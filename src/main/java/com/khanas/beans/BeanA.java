package com.khanas.beans;

import com.khanas.validator.BeanValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class BeanA implements BeanValidator, InitializingBean, DisposableBean {

    private Logger logger = LogManager.getLogger();

    private String name;
    private int value;

    public BeanA(final String name, final int value) {
        this.name = name;
        this.value = value;
    }

    public final String getName() {
        return name;
    }

    public final void setName(final String name) {
        this.name = name;
    }

    public final int getValue() {
        return value;
    }

    public final void setValue(final int value) {
        this.value = value;
    }

    @Override
    public final String toString() {
        return "BeanA{"
                + "name='" + name + '\''
                + ", value=" + value + '}';
    }

    public final void validate() {
        if (name == null) {
            logger.info("Name in BeanA cannot be null");
        }
        if (value < 0) {
            logger.info("Value in BeanA cannot be negative");
        }
    }

    @Override
    public final void destroy() throws Exception {
        logger.info("Destroy beanA");
    }

    @Override
    public final void afterPropertiesSet() throws Exception {
        logger.info("BeanA after properties set");
    }
}
