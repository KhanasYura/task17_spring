package com.khanas.beans;

import com.khanas.validator.BeanValidator;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class BeanValid implements BeanPostProcessor {
    @Override
    public final Object postProcessBeforeInitialization(final Object o, final String s) throws BeansException {
        return o;
    }

    @Override
    public final Object postProcessAfterInitialization(final Object o, final String s) throws BeansException {
        if (o instanceof BeanValidator) {
            ((BeanValidator) o).validate();
        }
        return o;
    }

    @Override
    public final String toString() {
        return "BeanValid";
    }
}
