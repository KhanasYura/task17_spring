package com.khanas.configuration;

import com.khanas.beans.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Lazy;

@Configuration
@Import(BeanConfigTwo.class)
public class BeanConfigOne {
    @Bean
    public BeanA getBeanA1(BeanB getBeanB, BeanC getBeanC) {
        return new BeanA(getBeanB.getName(), getBeanC.getValue());
    }

//    @Bean
//    public BeanA getBeanA2(BeanC getBeanC, BeanD getBeanD) {
//        return new BeanA(getBeanC.getName(), getBeanD.getValue());
//    }
//
//    @Bean
//    public BeanA getBeanA3(BeanB getBeanB, BeanD getBeanD) {
//        return new BeanA(getBeanB.getName(), getBeanD.getValue());
//    }

    @Bean
    public BeanE getBeanE1(BeanA getBeanA1) {
        return new BeanE(getBeanA1.getName(), getBeanA1.getValue());
    }

//    @Bean
//    public BeanE getBeanE2(BeanA getBeanA2) {
//        return new BeanE(getBeanA2.getName(), getBeanA2.getValue());
//    }
//
//    @Bean
//    public BeanE getBeanE3(BeanA getBeanA3) {
//        return new BeanE(getBeanA3.getName(), getBeanA3.getValue());
//    }

    @Bean(name = "beanF")
    @Lazy
    public BeanF getBeanF() {
        return new BeanF("Name6", 6);
    }

    @Bean
    public static MyBean getMyBean() {
        return new MyBean();
    }

    @Bean
    public BeanValid getBeanValid() {
        return new BeanValid();
    }


}
