package com.khanas.configuration;

import com.khanas.beans.BeanB;
import com.khanas.beans.BeanC;
import com.khanas.beans.BeanD;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("my.properties")
public class BeanConfigTwo {
    @Autowired
    private Environment environment;

    @Bean(name = "beanB", initMethod = "init", destroyMethod = "destroy")
    @DependsOn(value = {"beanD", "beanC"})
    public BeanB getBeanB() {
        return new BeanB(environment.getProperty("beanB.name"), Integer.parseInt(environment.getProperty("beanB.value")));
    }

    @Bean(name = "beanC", initMethod = "init", destroyMethod = "destroy")
    public BeanC getBeanC() {
        return new BeanC(environment.getProperty("beanC.name"), Integer.parseInt(environment.getProperty("beanC.value")));
    }

    @Bean(name = "beanD", initMethod = "init", destroyMethod = "destroy")
    public BeanD getBeanD() {
        return new BeanD(environment.getProperty("beanD.name"), Integer.parseInt(environment.getProperty("beanD.value")));
    }

}
